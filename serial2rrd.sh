#!/bin/bash

SERIAL="/dev/ttyACM0"

#stty -F $SERIAL raw cs8 115200 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts 
stty -F $SERIAL raw 115200 -hupcl



while true # loop forever
do
  inputline="" # clear input
 
  # Loop until we get a valid reading from AVR
  until inputline=$(echo $inputline | grep -e "^A0:")
  do
     inputline=$(head -n 1 < $SERIAL)
  done
	echo "$inputline"
	A0Line=`echo "$inputline" | cut -f 1 -d ","`
	A0=`echo "$A0Line" | cut -f 2 -d ":"`
	A0=${A0%$'\r'}
	A1Line=`echo "$inputline" | cut -f 2 -d ","`
	A1=`echo "$A1Line" | cut -f 2 -d ":"`
	A1=${A1%$'\r'}
	A2Line=`echo "$inputline" | cut -f 3 -d ","`
	A2=`echo "$A2Line" | cut -f 2 -d ":"`
	A2=${A2%$'\r'}
	A3Line=`echo "$inputline" | cut -f 4 -d ","`
	A3=`echo "$A3Line" | cut -f 2 -d ":"`
	A3=${A3%$'\r'}
	A4Line=`echo "$inputline" | cut -f 5 -d ","`
	A4=`echo "$A4Line" | cut -f 2 -d ":"`
	A4=${A4%$'\r'}
	A5Line=`echo "$inputline" | cut -f 6 -d ","`
	A5=`echo "$A5Line" | cut -f 2 -d ":"`
	A5=${A5%$'\r'}
	D2Line=`echo "$inputline" | cut -f 7 -d ","`
	D2=`echo "$D2Line" | cut -f 2 -d ":"`
	D2=${D2%$'\r'}
	D5Line=`echo "$inputline" | cut -f 8 -d ","`
	D5=`echo "$D5Line" | cut -f 2 -d ":"`
	D5=${D5%$'\r'}
	D10Line=`echo "$inputline" | cut -f 9 -d ","`
	D10=`echo "$D10Line" | cut -f 2 -d ":"`
	D10=${D10%$'\r'}
	rrdtool update sensors_A0.rrd N:$A0
	rrdtool update sensors_A1.rrd N:$A1
	rrdtool update sensors_A2.rrd N:$A2
	rrdtool update sensors_A3.rrd N:$A3
	rrdtool update sensors_A4.rrd N:$A4
	rrdtool update sensors_A5.rrd N:$A5
	rrdtool update sensors_D2.rrd N:$D2
	rrdtool update sensors_D5.rrd N:$D5
	rrdtool update sensors_D10.rrd N:$D10
done
