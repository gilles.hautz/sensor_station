#!/bin/bash

rrdtool create sensors_A0.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_A1.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_A2.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_A3.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_A4.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_A5.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_D2.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_D5.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400

rrdtool create sensors_D10.rrd --step 10 \
DS:value:GAUGE:600:0:50000 \
RRA:AVERAGE:0.5:1:1200 \
RRA:MIN:0.5:12:2400 \
RRA:MAX:0.5:12:2400 \
RRA:AVERAGE:0.5:12:2400
