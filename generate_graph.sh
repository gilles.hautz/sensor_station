TIMERANGE1="end-10m"
TIMERANGE2="end-24h"
TIMERANGE3="end-4w"
WIDTH="200"

rrdtool graph sensors_A0_t1.png --imgformat PNG \
	--end now --start $TIMERANGE1 \
	--width $WIDTH \
	DEF:value=sensors_A0.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A0 \: Gas' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A0_t2.png --imgformat PNG \
	--end now --start $TIMERANGE2 \
	--width $WIDTH \
	DEF:value=sensors_A0.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A0 \: Gas' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A0_t3.png --imgformat PNG \
	--end now --start $TIMERANGE3 \
	--width $WIDTH \
	DEF:value=sensors_A0.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A0 \: Gas' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A1_t1.png --imgformat PNG \
	--end now --start $TIMERANGE1 \
	--width $WIDTH \
	DEF:value=sensors_A1.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A1 \: Noise level' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A1_t2.png --imgformat PNG \
	--end now --start $TIMERANGE2 \
	--width $WIDTH \
	DEF:value=sensors_A1.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A1 \: Noise level' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A1_t3.png --imgformat PNG \
	--end now --start $TIMERANGE3 \
	--width $WIDTH \
	DEF:value=sensors_A1.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A1 \: Noise level' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A2_t1.png --imgformat PNG \
	--end now --start $TIMERANGE1 \
	--width $WIDTH \
	DEF:value=sensors_A2.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A2 \: Current sensor' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A2_t2.png --imgformat PNG \
	--end now --start $TIMERANGE2 \
	--width $WIDTH \
	DEF:value=sensors_A2.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A2 \: Current sensor' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A2_t3.png --imgformat PNG \
	--end now --start $TIMERANGE3 \
	--width $WIDTH \
	DEF:value=sensors_A2.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A2 \: Current sensor' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A3_t1.png --imgformat PNG \
	--end now --start $TIMERANGE1 \
	--width $WIDTH \
	DEF:value=sensors_A3.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A3 \: Light sensor' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A3_t2.png --imgformat PNG \
	--end now --start $TIMERANGE2 \
	--width $WIDTH \
	DEF:value=sensors_A3.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A3 \: Light sensor' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_A3_t3.png --imgformat PNG \
	--end now --start $TIMERANGE3 \
	--width $WIDTH \
	DEF:value=sensors_A3.rrd:value:AVERAGE \
	LINE2:value#0000FF:'A3 \: Light sensor' \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

##rrdtool graph sensors_A4.png --imgformat PNG \
#	--end now --start $TIMERANGE1 \
#	--width $WIDTH \
#	DEF:value=sensors_A4.rrd:value:AVERAGE \
#	LINE2:value#0000FF:'No sensor' \
#	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
#	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
#	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

#rrdtool graph sensors_A5.png --imgformat PNG \
#	--end now --start $TIMERANGE1 \
#	--width $WIDTH \
#	DEF:value=sensors_A5.rrd:value:AVERAGE \
#	LINE2:value#0000FF:'No sensor' \
#	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
#	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
#	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D2_t1.png --imgformat PNG \
	--end now --start $TIMERANGE1 \
	--width $WIDTH \
	DEF:value=sensors_D2.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D2 \: Geiger cpm" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D2_t2.png --imgformat PNG \
	--end now --start $TIMERANGE2 \
	--width $WIDTH \
	DEF:value=sensors_D2.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D2 \: Geiger cpm" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D2_t3.png --imgformat PNG \
	--end now --start $TIMERANGE3 \
	--width $WIDTH \
	DEF:value=sensors_D2.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D2 \: Geiger cpm" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D5_t1.png --imgformat PNG \
	--end now --start $TIMERANGE1 \
	--width $WIDTH \
	DEF:value=sensors_D5.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D5 \: DTH11 Humidity" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D5_t2.png --imgformat PNG \
	--end now --start $TIMERANGE2 \
	--width $WIDTH \
	DEF:value=sensors_D5.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D5 \: DTH11 Humidity" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D5_t3.png --imgformat PNG \
	--end now --start $TIMERANGE3 \
	--width $WIDTH \
	DEF:value=sensors_D5.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D5 \: DTH11 Humidity" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D10_t1.png --imgformat PNG \
	--end now --start $TIMERANGE1 \
	--width $WIDTH \
	DEF:value=sensors_D10.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D10 \: Temp °C" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D10_t2.png --imgformat PNG \
	--end now --start $TIMERANGE2 \
	--width $WIDTH \
	DEF:value=sensors_D10.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D10 \: Temp °C" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

rrdtool graph sensors_D10_t3.png --imgformat PNG \
	--end now --start $TIMERANGE3 \
	--width $WIDTH \
	DEF:value=sensors_D10.rrd:value:AVERAGE \
	LINE2:value#0000FF:"D10 \: Temp °C" \
	GPRINT:value:LAST:" Current\:%8.0lf %s"  \
	GPRINT:value:AVERAGE:"Average\:%8.0lf %s"  \
	GPRINT:value:MAX:"Maximum\:%8.0lf %s\n"  \

