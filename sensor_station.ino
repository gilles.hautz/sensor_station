// MQ6 Gaz sensor        -> A0
// BIG Sound sensor      -> A1
// Current sensor        -> A2
// Light sensor          -> A3
// Geiger counter        -> D2
// DTH11 Humidity sensor -> D5
// DS18B20 Temp sensor   -> D10
// KY016 3-color LED module ->
//                            -> D11
int redpin = 9; // select the pin for the red LED
int bluepin = 7; // select the pin for the blue LED
int greenpin = 8 ;// select the pin for the green LED


#include <SPI.h>
#include <OneWire.h> 
#include <DallasTemperature.h>
#include <dht.h>

#define ONE_WIRE_BUS 10
#define DHT11_PIN 5
#define STEP 10000 // 10s

OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature sensors(&oneWire);
dht DHT;

unsigned long counts;
unsigned long cpm;
unsigned long previousMillis;
unsigned int multiplier;

void setup(){
  Serial.begin(115200);
  sensors.begin();
  counts = 0;
  cpm = 0;
  multiplier = 60000 / STEP;
  attachInterrupt(0, tube_impulse, FALLING);
  pinMode (redpin, OUTPUT);
  pinMode (bluepin, OUTPUT);
  pinMode (greenpin, OUTPUT);
}

void tube_impulse(){
  counts++;
}

void loop(){
  analogWrite (greenpin, 255);
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis > STEP){
    previousMillis = currentMillis;
    sensors.requestTemperatures();
    cpm = counts * multiplier;
    counts = 0;
    int chk = DHT.read11(DHT11_PIN);


    
    Serial.print("A0:");
    Serial.print(analogRead(A0));
    Serial.print(",");
    Serial.print("A1:");
    Serial.print(analogRead(A1));
    Serial.print(",");
    Serial.print("A2:");
    Serial.print(analogRead(A2));
    Serial.print(",");
    Serial.print("A3:");
    Serial.print(analogRead(A3));
    Serial.print(",");
    Serial.print("A4:");
    Serial.print(analogRead(A4));
    Serial.print(",");
    Serial.print("A5:");
    Serial.print(analogRead(A5)); 
    Serial.print(",");
    Serial.print("D2:");
    Serial.print(cpm);
    Serial.print(",");
    Serial.print("D5:");
    Serial.print(DHT.humidity);
    Serial.print(",");
    Serial.print("D10:");
    Serial.println(sensors.getTempCByIndex(0));
    analogWrite (redpin, 255);
    analogWrite (greenpin, 0);
    analogWrite (bluepin, 255);
    delay(500);
  }
}
